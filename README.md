# Användare

##

## Terminalkommandon i Python

Med hjälp av modulen `subprocess` kan man köra ett kommando i terminalen
inifrån Python, och funktionen `check_output` returnerar svaret från
kommandot. Funktionen tar en lista med argument som bygger upp kommandot:
till exempel `data = subprocess.check_output(['python3', 'test.py'])`.

Returvärdet för funktionen är av typen `bytes`, vilket är precis vad det
låter som: en sekvens av bytes (heltal mellan 0 och 255). För att göra om
det till en mer hanterlig sträng används funktionen decode:
`data_str = bytes.decode(data, 'utf-8')`. (UTF-8 är ett sätt att
representera tecken som heltal.)

## En dictionary av användare

Två av de fält som returneras av `getent passwd` är användarnamn och
riktigt namn. Skriv en funktion som tar in utdata från kommandot och
returnerar en dictionary där varje användarnamn (nyckel) hör ihop med
motsvarande riktiga namn (värde).

## Användarobjekt

Varje användarnamn hör ju ihop inte bara med ett riktigt namn, men också
med en hel del annan data. Hur representerar man all denna data i ett
enda objekt på ett bra sätt?

Här fungerar en dictionary bra. Nycklarna blir namnen på attributen,
medan värdena blir attributen själva. Till exempel skulle följande
rad från `getent passwd`:

```
sebastian:*:1500:100:Sebastian Ekström:/home/sebastian:/bin/bash
```

kunna representeras av följande dictionary:

```python
{'group id': 100,
 'home directory': '/home/sebastian',
 'password': '*',
 'real name': 'Sebastian Ekström',
 'shell': '/bin/bash',
 'user id': 1500,
 'username': 'sebastian'}
```
 
Sedan kan dessa användardictionaries läggas i ännu en dictionary, med
användarnamnen som nycklar. På så sätt kan man lätt slå upp information
om vilken användare som helst, bara man har deras användarnamn:

```python
>>> users['sebastian']['user id']
1500
```

Skriv en funktion som tar in utdatan från `getent passwd` och skapar
en sådan dictionary över användare. Kom ihåg att du kan använda strängmetoderna
`splitlines()` och `split(separator)` för att dela upp en sträng i bitar.

# Processer

## 

## Skapa processobjekt

Nu har du tillräckligt med information för att kunna ta fram en sträng
som innehåller en lista med alla processer. Men att ha all information i
en enda sträng är inte särskilt praktiskt om man till exempel vill ha
reda på detaljerna för en specifik process. Att låta varje process vara
ett eget objekt skulle vara mycket mer användbart, men vad för
sorts objekt ska man använda?

Även här skulle en dictionary passa bra. Precis som tidigare blir
nycklarna namnen på attributen, och värdena blir attributen själva:

```
zabbix      1426  1417 zabbix_agentd
```

blir

```
{'COMMAND': 'zabbix_agentd',
 'PID': 1426,
 'PPID': 1417,
 'USER': 'zabbix'}
```

Sedan kan man även här lägga dessa dictionaries i ännu en dictionary, med process-ID
som nycklar. En lista skulle här inte vara speciellt praktisk: eftersom inte alla
ID-nummer motsvarar en pågående process, kan man inte lägga en process
på platsen som motsvarar dess ID-nummer utan att lämna tomma platser i
listan, och om ID-numret inte motsvarar platsen blir det svårt att hitta
en specifik process.

Skriv en funktion som skapar en sådan dictionary av processer från den
output du får från `ps -eo user:10,pid,ppid,comm --noheader`.

## Processträd

Ett annat kommando som skriver ut pågående processer är `pstree`. Som du
ser om du skriver in det ger detta kommando en utskrift som är mycket
annorlunda än den från `ps -ef`: istället för en lista så är utskriften
en sorts graf där varje process är kopplad till den process som startat
den. Denna typ av datastruktur kallas för ett träd, och din nästa
uppgift är att skapa en liknande struktur.

Ett av fälten i dina process-dictionaries är PPID, parent process ID,
det vill säga vilken annan process som startade processen. Skriv en
funktion som går igenom alla dina processobjekt och använder PPID-fältet
för att skapa ett nytt fält: en lista som innehåller alla andra processer
(dictionary-objekten själva, inte bara deras ID) som denna process har
startat. Om en process har PPID 0 betyder det att den inte har startats
av någon annan process. Dessa processer ska läggas i en särskild lista,
som funktionen ska returnera när den är klar: var och en av dem är roten
till ett processträd, och du kommer att använda dem för att gå igenom
träden i nästa uppgift.

## Utskrift

Nu när vi har vårt träd är nästa steg att skriva ut det. Men om man
skriver ut all information om alla processer blir trädet väldigt
oöverskådligt, så därför nöjer vi oss med att skriva ut processernas
ID och namn. Du behöver inte ha med linjerna som `pstree` använder, utan
det räcker att du använder indrag (till exempel med \t) för att ange
vilken "nivå" av trädet en process ligger på: till exempel

```
1: init
	310: upstart-udev-br
    1216: sshd
		10660: sshd
```

där 310 och 1216 startades av 1, och 10660 startades av 1216.

Tips: Man kan lätt gå igenom ett helt träd med hjälp av rekursion. Om
du har en funktion som skriver ut informationen för en process, kan du
avsluta funktionen genom att loopa genom listan med alla processer som
din process har startat och anropa funktionen på dem:

```python
def traverse_tree(node):
    # do things with node here
    for child_node in children:
        traverse_tree(child_node)
```
