import subprocess
import pprint

def make_dict(raw_data):
    raw_data = raw_data.split(':')
    raw_data = '\n'.join(raw_data).split('\n')
    return_dict = {}
    for index in range(len(raw_data)):
        if index % 7 == 0:
            try:
                return_dict[raw_data[index]] = raw_data[index + 4]
            except IndexError:
                return return_dict
    return return_dict

def nicer_dict(raw_data):
    raw_data = raw_data.split(':')
    raw_data = '\n'.join(raw_data).split('\n')
    return_dict = {}
    poss = 0
    user = ''
    things = ['username', 'password', 'user id', 'group id', 'real name', 'home directory', 'shell']
    for element in raw_data:
        if poss == 0:
            user = element
            return_dict[user] =  {}
        return_dict[user][things[poss]] = element
        poss += 1
        if poss == 7:
            poss = 0
    return return_dict

if __name__ == '__main__':
    raw_data = bytes.decode(subprocess.check_output(['getent', 'passwd']), 'utf-8')
    #print(make_dict(raw_data))
    print(nicer_dict(raw_data))
